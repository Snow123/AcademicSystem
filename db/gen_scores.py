import pymysql
from numpy import random as R

num_exams = 10
num_students = 100
num_subjects = 8

statement = 'insert into scores (exam_id, student_id, subject_id, score) values({},{},{},{});'

conn = pymysql.connect(
    host='localhost',
    user='root',
    password='898213141310',
    db='media')

with conn.cursor() as cur:
    for i in range(num_exams):
        for j in range(num_students):
            for k in range(num_subjects):
                s = statement.format(i, j, k, R.randint(60, 100))
                print(s)
                cur.execute(s)

conn.commit()
conn.close()
