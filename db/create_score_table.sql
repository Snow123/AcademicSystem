use media;
drop table if exists scores;
create table scores (
    id bigint(20) not null auto_increment,
    exam_id int(10) not null,
    student_id bigint(20) not null,
    subject_id int(10) not null,
    score float(5, 2) not null default 0,
    primary key(id)
);
