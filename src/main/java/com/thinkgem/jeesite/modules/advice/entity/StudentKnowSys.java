package com.thinkgem.jeesite.modules.advice.entity;

import com.thinkgem.jeesite.common.persistence.DataEntity;

public class StudentKnowSys extends DataEntity<StudentKnowSys>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String studentAccount;

	public String getStudentAccount() {
		return studentAccount;
	}

	public void setStudentAccount(String studentAccount) {
		this.studentAccount = studentAccount;
	}

	public String getQuestionCode() {
		return questionCode;
	}

	public void setQuestionCode(String questionCode) {
		this.questionCode = questionCode;
	}

	public String getKnowlegeName() {
		return knowlegeName;
	}

	public void setKnowlegeName(String knowlegeName) {
		this.knowlegeName = knowlegeName;
	}

	public String getKnowlegeClass() {
		return knowlegeClass;
	}

	public void setKnowlegeClass(String knowlegeClass) {
		this.knowlegeClass = knowlegeClass;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	private String questionCode;
	
	private String knowlegeName;
	
	private String knowlegeClass;
	
	private int value;
	
	private int score;
	
}
