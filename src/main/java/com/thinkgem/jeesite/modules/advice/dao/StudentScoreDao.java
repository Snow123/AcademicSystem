package com.thinkgem.jeesite.modules.advice.dao;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.advice.entity.StudentScoreSearch;

@MyBatisDao
public interface StudentScoreDao extends CrudDao<StudentScoreSearch>{
	
	List<StudentScoreSearch> getScore(StudentScoreSearch search);
	List<StudentScoreSearch> getTerm();
	String getFirstYear(String studentAccount) ;
	List<StudentScoreSearch> getExam(StudentScoreSearch search);
	List<StudentScoreSearch> getSubject(StudentScoreSearch search);
}
