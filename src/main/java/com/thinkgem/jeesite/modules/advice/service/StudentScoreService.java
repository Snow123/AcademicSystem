package com.thinkgem.jeesite.modules.advice.service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.service.CrudService;
import com.thinkgem.jeesite.modules.advice.dao.StudentKnowSysDao;
import com.thinkgem.jeesite.modules.advice.dao.StudentScoreDao;
import com.thinkgem.jeesite.modules.advice.dao.StudentScoreFollowDao;
import com.thinkgem.jeesite.modules.advice.entity.StudentKnowSys;
import com.thinkgem.jeesite.modules.advice.entity.StudentScoreFollow;
import com.thinkgem.jeesite.modules.advice.entity.StudentScoreSearch;
import com.thinkgem.jeesite.modules.sys.utils.UserUtils;

/**
 * @author yangping
 * 学生端service
 * 
 *
 */
@Service
@Transactional(readOnly = true)
public class StudentScoreService  extends CrudService<StudentScoreDao, StudentScoreSearch>{
	@Autowired
	private StudentScoreDao studentDao;
	
	@Autowired
	private StudentKnowSysDao studentKnowSysDao;
	

	@Autowired
	private StudentScoreFollowDao studentScoreFollowDao;
	
	/**
	 * 成绩查询
	 * @return
	 */
	public  List<StudentScoreSearch> getScore(StudentScoreSearch search) {
		String loginName = UserUtils.getUser().getLoginName();
		search.setStudentAccount(loginName);
		List<StudentScoreSearch> list = this.studentDao.getScore(search);
		if (list == null || list.size() == 0) {
			return new ArrayList<StudentScoreSearch>();
		}
		return list;
		
	}
	/**
	 * 知识点分析
	 * @return
	 */
	public  List<StudentKnowSys> getKnowSys(StudentScoreSearch search) {
		String loginName = UserUtils.getUser().getLoginName();
		search.setStudentAccount(loginName);
		List<StudentKnowSys> list = this.studentKnowSysDao.getKnowSys(search);
		if (list == null || list.size() == 0) {
			return new ArrayList<StudentKnowSys>();
		}
		return list;
	}
	
	public List<StudentScoreSearch> getTerm(){
		String loginName = UserUtils.getUser().getLoginName();
		String firstYear = this.studentDao.getFirstYear(loginName);
		firstYear = firstYear + "-08-25";
		int grade = getGrade(firstYear);
		List<StudentScoreSearch> list = new ArrayList<StudentScoreSearch>();
		if (1== grade) {
			StudentScoreSearch scoreSearch = new StudentScoreSearch();
			scoreSearch.setTermCode("G11");
			scoreSearch.setTermName("高一上学期");
			list.add(scoreSearch);
			scoreSearch = new StudentScoreSearch();
			scoreSearch.setTermCode("G12");
			scoreSearch.setTermName("高一下学期");
			list.add(scoreSearch);
			
		}else if(2== grade) {
			StudentScoreSearch scoreSearch = new StudentScoreSearch();
			scoreSearch.setTermCode("G21");
			scoreSearch.setTermName("高二上学期");
			list.add(scoreSearch);
			scoreSearch = new StudentScoreSearch();
			scoreSearch.setTermCode("G22");
			scoreSearch.setTermName("高二下学期");
			list.add(scoreSearch);
			
		}else if(2== grade){
			StudentScoreSearch scoreSearch = new StudentScoreSearch();
			scoreSearch.setTermCode("G31");
			scoreSearch.setTermName("高三上学期");
			list.add(scoreSearch);
			scoreSearch = new StudentScoreSearch();
			scoreSearch.setTermCode("G32");
			scoreSearch.setTermName("高三下学期");
			list.add(scoreSearch);
		}
		return list;
	}
	
	public List<StudentScoreSearch> getExam (String termCode){
		String loginName = UserUtils.getUser().getLoginName();
		StudentScoreSearch param = new StudentScoreSearch();
		param.setTermCode(termCode);
		param.setStudentAccount(loginName);
		return this.studentDao.getExam(param);
		
	} 
	
	public List<StudentScoreSearch> getSubject (String termCode){
		StudentScoreSearch param = new StudentScoreSearch();
		param.setTermCode(termCode);
		return this.studentDao.getSubject(param);
	}
	private int getGrade(String startDate) {
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");  
		  
        Date date = null;
		try {
			date = format.parse(startDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}  
        //开始时间  
        Calendar start = Calendar.getInstance();  
        start.setTime(date);  
        //结束时间  
        Calendar end = Calendar.getInstance();  
        int subMonthCount = 0;
        if (!start.after(end)){  
            subMonthCount =   
                    (end.get(Calendar.YEAR) - start.get(Calendar.YEAR) == 0)   
                            ? end.get(Calendar.MONTH) - start.get(Calendar.MONTH)  //同一年  
                    :   ((end.get(Calendar.YEAR) - start.get(Calendar.YEAR) >= 2) //年数差超过2年  
                                ? (end.get(Calendar.YEAR) - start.get(Calendar.YEAR) - 1)   
                                    * 12 + start.getActualMaximum(Calendar.MONTH) - start.get(Calendar.MONTH)   
                                    + end.get(Calendar.MONTH) + 1  
                                : start.getActualMaximum(Calendar.MONTH) - start.get(Calendar.MONTH)   
                                    + end.get(Calendar.MONTH) + 1);  //年数差为1，Calendar.get(MONTH) 第一月是0，所以+1  
            System.out.println(subMonthCount);  
        }
		return subMonthCount/12 +1;  
	}
	
	/**
	 * 成绩追踪
	 * @return
	 */
	public Map<String, Object> follow(String key) {
		List<StudentScoreFollow> list = new ArrayList<StudentScoreFollow>();
		StudentScoreFollow param = new StudentScoreFollow();
		param.setStudentAccount(UserUtils.getUser().getLoginName());
		if("allScore".equals(key)) {
			list = this.studentScoreFollowDao.getFollowByAll(param);
		}else {
			param.setSubjectId(Integer.valueOf(key));
			list = this.studentScoreFollowDao.getFollowBySubject(param);
		}
		List<String> x = new ArrayList<String>();
		List<Float> y = new ArrayList<Float>();
		for (StudentScoreFollow follow : list) {
			x.add(follow.getExamDate());
			y.add(follow.getStandardScore());
		}
		Map<String, Object> map = new HashMap<String,Object>();
		map.put("x", x);
		map.put("y", y);
		return map;
	}
}
