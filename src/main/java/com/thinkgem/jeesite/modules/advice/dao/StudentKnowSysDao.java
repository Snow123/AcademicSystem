package com.thinkgem.jeesite.modules.advice.dao;

import java.util.List;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.advice.entity.StudentKnowSys;
import com.thinkgem.jeesite.modules.advice.entity.StudentScoreSearch;

@MyBatisDao
public interface StudentKnowSysDao extends CrudDao<StudentKnowSys>{
	
	List<StudentKnowSys> getKnowSys(StudentScoreSearch search);
}
