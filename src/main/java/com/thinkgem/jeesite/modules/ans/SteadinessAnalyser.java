package com.thinkgem.jeesite.modules.ans;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;

import java.util.ArrayList;

/**
 * 稳定性分析
 */
public final class SteadinessAnalyser {

    /**
     * 为一个学生学科标准差
     * @param scores NxC 成绩，N为考试数，C为科目数
     * @return Cx1标准差
     */
    public static float[] getScoresStd(float[][] scores){
        INDArray S = Nd4j.create(scores);
        INDArray std = S.std(0);
        return std.data().asFloat().clone();
    }

    /**
     * 计算学科稳定性
     * @param scoresStd NxC 标准差，N为学生数，C为科目数
     * @return NxC 学科稳定性
     */
    public static ArrayList<float[]> getSubjectSteadiness(float[][] scoresStd){
        float[][] R = getSubjectSteadinessP(scoresStd);
        ArrayList<float[]> SS = new ArrayList<float[]>();
        for(int i = 0; i < R.length; i++)
            SS.add(R[i]);
        return SS;
    }

    public static float[][] getSubjectSteadiness2(float[][] scoresStd){
        INDArray std_scores = Nd4j.create(scoresStd);
        int row = std_scores.rows();
        int col = std_scores.columns();

        float[][] SS = new float[row][col];

        ImmutablePair<int[][], float[][]> sorted =
            NumericUtils.ArgSort(std_scores, 1, true);

        for(int i = 0; i < row; i++){
            for(int j = 0; j < col; j++)
                SS[i][j] = sorted.left[i][j]+1;
        }

        return SS;
    }

    public static float[][] getSubjectSteadinessP(float[][] scoresStd){
        INDArray stdScores = Nd4j.create(scoresStd);
        int row = stdScores.rows();
        int col = stdScores.columns();
        float[][] SS = new float[row][col];

        ImmutablePair<int[][], float[][]> sorted = 
            NumericUtils.ArgSort(stdScores, 0, true);
        
        for(int i = 0; i < row; i++){
            for(int j = 0; j < col; j++)
                SS[i][j] = sorted.left[i][j]+1;
        }

        return SS;
    }

    /**
     * 计算组合稳定性
     * @param steady NxC 学科稳定性，N为学生数，C为科目数
     * @param combinations MxD 学科ID组合, M为组合数, D为每组内包含的组合数
     * @return NxCC 组合稳定性，N为学生数，CC为组合数
     */
    public static ArrayList<float[]> getCombinedSteadiness(float[][] steady, int[][] combinations){
        INDArray ranking = Nd4j.create(steady);
        int combNum = combinations.length;
        int studentNum = ranking.rows();
        INDArray combined = Nd4j.zeros(ranking.rows(), combNum);

        for(int i = 0; i < combNum; i++){
            int[] comb = combinations[i];
            for(int j = 0; j < comb.length; j++)
                comb[j] -= 1;
            combined.putColumn(i, ranking.getColumns(comb).mean(1));
        }

        ArrayList<float[]> result = new ArrayList<>();
        for(int i = 0; i < studentNum; i++){
            INDArray row = combined.getRow(i).dup();
            result.add(row.data().asFloat());
        }

        return result;
    }

    public static float[][] getCombinedSteadinessP(float[][] steady, int[][] combinations){
        INDArray ranking = Nd4j.create(steady);
        int combNum = combinations.length;
        int studentNum = ranking.rows();
        INDArray combined = Nd4j.zeros(ranking.rows(), combNum);

        for(int i = 0; i < combNum; i++){
            int[] comb = combinations[i];
            for(int j = 0; j < comb.length; j++)
                comb[j] -= 1;
            combined.putColumn(i, ranking.getColumns(comb).mean(1));
        }

        float[][] result = new float[studentNum][];
        for(int i = 0; i < studentNum; i++){
            INDArray row = combined.getRow(i).dup();
            result[i] = row.data().asFloat();
        }

        return result;
    }
}
