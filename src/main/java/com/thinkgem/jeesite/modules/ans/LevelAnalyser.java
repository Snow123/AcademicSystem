package com.thinkgem.jeesite.modules.ans;

import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.nd4j.linalg.indexing.*;

import org.apache.commons.lang3.tuple.ImmutablePair;

import java.util.ArrayList;

/**
 * 水平分析
 */
public final class LevelAnalyser {

    public static float[] Edges = new float[]{0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100};

    /**
     * 带难度加权的标准分数计算
     * @param scores NxC 成绩矩阵，N为学生数，C为科目数
     * @param diff 难度系数 C 个
     * @return NxC 的标准化成绩
     */
    public static float[][] NormalizeScore(float[][] scores, float[] diff){
        INDArray scoresPerAssign = Nd4j.create(scores);
        int student_num = scoresPerAssign.rows();
        INDArray difficulty = Nd4j.create(diff)
                                  .repeat(0, scoresPerAssign.rows());

        INDArray weightedScores = scoresPerAssign.mul(difficulty);
        INDArray mean = weightedScores.mean(0).repeat(0, weightedScores.rows());
        INDArray std = weightedScores.std(0).repeat(0, weightedScores.rows());

        INDArray normalized =  weightedScores.sub(mean).div(std);

        float[][] result = new float[student_num][];
        for(int i = 0; i < normalized.rows(); i++){
            INDArray row = normalized.getRow(i).dup();
            result[i] = row.data().asFloat();
        }
        return result;
    }

    /**
     * 不带难度加权的Z分数
     */
    public static float[][] NormalizeScoreWithoutDiff(float[][] scores){
        INDArray scoresPerAssign = Nd4j.create(scores);
        
        float[][] result = NormalizeScoreWithoutDiff(scoresPerAssign);
        return result;
    }

    public static float[][] NormalizeScoreWithoutDiff(INDArray scores){
        int student_num = scores.rows();
        INDArray mean = scores.mean(0).repeat(0, scores.rows());
        INDArray std = scores.std(0).repeat(0, scores.rows());
        INDArray normalized =  scores.sub(mean).div(std);

        float[][] result = new float[student_num][];
        for(int i = 0; i < normalized.rows(); i++){
            INDArray row = normalized.getRow(i).dup();
            result[i] = (row.data().asFloat());
        }
        return result;
    }

    /**
     * 标准化总成绩
     * @param scores N个学生的总成绩
     * @return N个学生的标准分数
     */
    public static float[] NormalizeScore(float[] scores){
        INDArray S = Nd4j.create(scores);
        INDArray mean = Nd4j.repeat(Nd4j.mean(S), scores.length);
        INDArray std = Nd4j.repeat(Nd4j.std(S), scores.length);
        INDArray normalized = S.sub(mean).div(std);

        return normalized.data().asFloat();
    }

    /**
     * 组合的Z分数
     * @param scores NxC 考试成绩，N为学生数，C为科目数
     * @param combinations CCxM组合索引
     * @return NxCC 组合Z分数
     */
    public static float[][] getCombinationNormalized(
        float[][] scores,
        int[][] combinations){
        INDArray S = Nd4j.create(scores);
        int num_comb = combinations.length;
        INDArray[] columns = new INDArray[num_comb];

        // 获取每一个组合列
        for(int i = 0; i < num_comb; i++){
            int[] comb = combinations[i];
            columns[i] = S.getColumns(comb).sum(1);
        }

        INDArray combined =  Nd4j.hstack(columns);
        float [][] result = NormalizeScoreWithoutDiff(combined);
        
        return result;
    }

    /**
     * 对一个学生计算相对水平
     * @param scores NxC 标准化分数，N为考试数，C为科目数
     * @return Cx1 学生相对水平
     */
    public static float[] getRelativeLevel(float[][] scores){
        INDArray normalized = Nd4j.create(scores);

        INDArray mean = normalized.mean(0);

        return mean.data().asFloat().clone();
    }

    public static float[] getRelativeLevel(float[][] scores, float[][] diff){
        INDArray normalized = Nd4j.create(scores);
        INDArray difficulty = Nd4j.create(diff);
        
        INDArray weighted = normalized.mul(difficulty);
        INDArray mean = weighted.mean(0);

        return mean.data().asFloat().clone();
    }

    /**
     * 对一个学生计算学科组合水平
     * @param scores NxC 标准化分数，N为考试数，C为科目数
     * @param combinations MxD 学科ID组合, M为组合数, D为每组内包含的组合数
     * @return Cx1 C个组合成绩水平
     */
    public static ArrayList<Float> getCombinationLevel(float[][] scores, int[][] combinations){
        INDArray scoresPerAssign = Nd4j.create(scores);
        ArrayList<Float> C = new ArrayList<>();

        for (int[] comb : combinations) {
            for(int i = 0; i < comb.length; i++)
                comb[i] -= 1;
            float val = scoresPerAssign
                    .getColumns(comb)
                    .meanNumber()
                    .floatValue();
            C.add(val);
        }

        return C;
    }

    /**
     * 对一个学生计算学科组合水平
     * @param scores NxC 标准化分数，N为考试数，C为科目数
     * @param diff Cx1 科目难度系数
     * @param combinations MxD 学科ID组合, M为组合数, D为每组内包含的组合数
     * @return Cx1 C个组合成绩水平
     */
    public static float[] getCombinationLevel(float[][] scores, float[][] diff, int[][] combinations){
        INDArray scoresPerAssign = Nd4j.create(scores);
        float[] C = new float[combinations.length];

        INDArray difficulty = Nd4j.create(diff);
        INDArray weightedScores = scoresPerAssign.mul(difficulty);

        for (int i = 0; i < combinations.length; i++) {
            int[] comb = combinations[i];
            for(int j = 0; j < comb.length; j++)
                comb[j] -= 1;
            float val = weightedScores
                    .getColumns(comb)
                    .meanNumber()
                    .floatValue();
            C[i] = val;
        }

        return C;
    }

    /**
     * 计算组合选择排名
     * @param combscores NxCC N个学生的CC种组合标准分
     * 
     */
    public static ImmutablePair<int[], int[][]> getCombinedSelection(float[][] combscores){
        int student_num = combscores.length, comb_num = combscores[0].length;
        INDArray scores = Nd4j.create(combscores);

        ImmutablePair<int[][], float[][]> R = NumericUtils.ArgSort(combscores, 0, false);

        int[][] I = R.left;
        int[] prior = new int[student_num];
        int[][] zorder = new int[student_num][comb_num];

        // Argmax
        for(int i = 0; i < student_num; i++){
            int min = Integer.MAX_VALUE;
            for(int j = 0; j < comb_num; j++){
                if(I[i][j] < min){
                    min = I[i][j];
                    prior[i] = j;
                }
            }
        }

        INDArray prior_vals = scores.get(
            NDArrayIndex.interval(0, student_num),
            new NDArrayIndex(prior));

        for(int i = 0; i < student_num; i++){
            for(int c = 0; c < comb_num; c++){
                int count = 1;
                float himself = scores.getFloat(i, c);

                // Assuming that student i selected c combination
                // then count the number of student that has c prior
                // combination.
                for(int j = 0; j < student_num; j++){
                    if(i != j
                    && prior[j] == c
                    && himself < prior_vals.getFloat(j)){
                        
                        count ++;
                    }
                }

                zorder[i][c] = count;
            }
        }

        return new ImmutablePair<>(prior, zorder);
    }

    /**
     * 计算直方分布
     * @param x 输入变量
     * @param edges 划分边界：n+1个时，生成n个结果，最后一个作为上限边界
     * @return (bins个数, idx每个变量在哪个分组)
     */
    public static ImmutablePair<int[], int[]> CalcHistogram(float[] x, float[] edges){
        int[] count = new int[edges.length - 1];
        int[] idx = new int[x.length];
        for(int j = 0; j < x.length; j++){
            for(int i = 1; i < edges.length; i++){
                if(x[j] > edges[i-1] && x[j] <=edges[i]){
                    idx[j] = i-1;
                    count[i-1] ++;
                }
            }
        }
        return new ImmutablePair<>(count, idx);
    }

    /**
     * 计算百分等级
     * @param scores N个学生的分数
     * @return N个学生的 百分等级
     */
    public static float[] PercentageLevel(float[] scores){
        int N = scores.length;
        float[] Pr = new float[N];

        // Calculate histogram for each range
        ImmutablePair<int[], int[]> hist = CalcHistogram(scores, Edges);
        int[] bins = hist.left, idx = hist.right;

        // Cumulate counts for lower bins.
        int[] cum = new int[bins.length];
        cum[0] = bins[0];
        for(int i = 1; i < bins.length; i++)
            cum[i] = cum[i-1] + bins[i];

        for(int i = 0; i < idx.length; i++){
            int Fb = cum[idx[i]-1];
            int f = bins[idx[i]];
            float Lb = Edges[idx[i]];

            Pr[i] = 100.f / N * (Fb + f * (scores[i] - Lb) / 10.f);
        }

        return Pr;
    }
}
