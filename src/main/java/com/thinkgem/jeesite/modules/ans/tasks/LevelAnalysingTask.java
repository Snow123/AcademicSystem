package com.thinkgem.jeesite.modules.ans.tasks;

import org.springframework.stereotype.Service;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.poi.ss.formula.eval.NotImplementedException;
import org.springframework.scheduling.annotation.Async;

import java.sql.Connection;
import java.util.concurrent.CompletableFuture;

import com.thinkgem.jeesite.modules.ans.*;

import org.slf4j.LoggerFactory;
import org.slf4j.Logger;

/**
 * LevelAnalysingTask
 */
@Service
public class LevelAnalysingTask {
    private DbUtils db_utils;
    private Logger logger;

    public LevelAnalysingTask(Connection conn){
        db_utils = new DbUtils(conn);
        logger = LoggerFactory.getLogger(getClass());
    }

    public DbUtils getDbUtils(){
        return db_utils;
    }

    @Async
    public CompletableFuture<ImmutablePair<int[], float[][]>>
    ApplyNormalizeScoreOfGrade(int examId){
        long start = System.currentTimeMillis(), end = 0;

        ImmutablePair<int[], float[][]> scores = db_utils.getScoresByExam(examId);
        end = System.currentTimeMillis();
        logger.info("[Analysing][Normalizing]Query costs {}ms", end - start);

        start = end;
        float[][] normalized = LevelAnalyser.NormalizeScoreWithoutDiff(scores.right);
        end = System.currentTimeMillis();
        logger.info("[Analysing][Normalizing]Computing costs {}ms", end-start);

        start = end;
        ImmutablePair<int[], float[][]> result = new ImmutablePair<int[], float[][]>(scores.left, normalized);
        db_utils.saveNormalizedScores(examId, new ImmutablePair<int[],float[][]>(scores.left, normalized));
        end = System.currentTimeMillis();
        logger.info("[Analysing][Normalizing]Saving costs {}ms", end-start);

        return CompletableFuture.completedFuture(result);
    }

    @Async
    public CompletableFuture<ImmutablePair<int[], float[][]>>
    ApplyNormalizedScoreOfClass(int examId){
        throw new NotImplementedException("ApplyNormalizedScoreOfClass");
    }

    @Async
    public CompletableFuture<float[]> ApplyCombinedLevel(int studentId){
        long start = System.currentTimeMillis(), end = 0;

        float[][] scores = db_utils.getNormalizedScoresByStudent(studentId);

        ImmutablePair<int[], int[][]> C = db_utils.getAllCombinations();
        int[] comb_ids = C.left;
        int[][] combinations = C.right;
        float[][] diff = db_utils.getExamDiffsByStudent(studentId);

        end = System.currentTimeMillis();
        logger.info("[Analysing][CombinedLevel]Query costs {}ms", end-start);

        start = end;
        float[] levels = LevelAnalyser.getCombinationLevel(scores, diff, combinations);
        end = System.currentTimeMillis();
        logger.info("[Analysing][CombinedLevel]Computing costs {}ms", end-start);

        start = end;
        db_utils.saveCombinationLevels(studentId, levels, comb_ids);
        end = System.currentTimeMillis();
        logger.info("[Analysing][CombinedLevel]Saving costs {}ms", end-start);
        
        return CompletableFuture.completedFuture(levels);
    }

    @Async
    public CompletableFuture<float[]> ApplyCombinedLevel(int studentId, float[][] diff){
        float[][] scores = db_utils.getNormalizedScoresByStudent(studentId);

        ImmutablePair<int[], int[][]> C = db_utils.getAllCombinations();
        int[] comb_ids = C.left;
        int[][] combinations = C.right;

        float[] levels = LevelAnalyser.getCombinationLevel(scores, diff, combinations);

        db_utils.saveCombinationLevels(studentId, levels, comb_ids);
        
        return CompletableFuture.completedFuture(levels);
    }

    @Async
    public CompletableFuture<float[]> ApplyRelativeLevel(int studentId, float[][] diff){
        float[][] scores = db_utils.getNormalizedScoresByStudent(studentId);
        float[] levels = LevelAnalyser.getRelativeLevel(scores, diff);

        db_utils.saveRelativeLevels(studentId, levels);

        return CompletableFuture.completedFuture(levels);
    }

    @Async
    public CompletableFuture<float[]> ApplyRelativeLevel(int studentId){
        long start = System.currentTimeMillis(), end = 0;
    
        float[][] scores = db_utils.getNormalizedScoresByStudent(studentId);
        float[][] diff = db_utils.getExamDiffsByStudent(studentId);
        end = System.currentTimeMillis();
        logger.info("[Analysing][RelativeLevel]Query costs {}ms", end-start);

        start = end;
        float[] levels = LevelAnalyser.getRelativeLevel(scores, diff);
        end = System.currentTimeMillis();
        logger.info("[Analysing][RelativeLevel]Computing costs {}ms", end-start);

        start = end;
        db_utils.saveRelativeLevels(studentId, levels);
        end = System.currentTimeMillis();
        logger.info("[Analysing][RelativeLevel]Saving costs {}ms", end-start);

        return CompletableFuture.completedFuture(levels);
    }

    @Async
    public ImmutablePair<int[], int[][]> ApplyCombinedSelection(int examId){
        long start = System.currentTimeMillis(), end = 0;

        // left: CC combination ids, right: NxCC combined levels
        ImmutablePair<int[], float[][]> C = db_utils.getCombinedLevelsByExam(examId);
        end = System.currentTimeMillis();
        logger.info("[Analysing][CombinedSelection]Query costs {}ms", end-start);

        // left: N prior combination, right: NxCC ranked combinations
        start = end;
        ImmutablePair<int[], int[][]> R =  LevelAnalyser.getCombinedSelection(C.right);
        end = System.currentTimeMillis();
        logger.info("[Analysing][CombinedSelection]Computing costs {}ms", end-start);

        start = end;
        db_utils.saveCombinedSelection(examId, C.left, R.left, R.right);
        end = System.currentTimeMillis();
        logger.info("[Analysing][CombinedSelection]Computing costs {}ms", end-start);

        return R;
    }
}
