package com.thinkgem.jeesite.modules.ans;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.apache.commons.math3.stat.regression.SimpleRegression;


public final class NumericUtils {

    /**
     * 一维排序
     * @param x 一维数组
     * @return (排名, 排序结果)
     */
    public static ImmutablePair<int[], float[]> ArgSort(float[] x){
        INDArray X = Nd4j.create(x);
        INDArray[] r = Nd4j.sortWithIndices(X, 1, false);
        int[] indices = new int[X.length()];
        for(int i = 0; i < r[0].length(); i++)
            indices[r[0].getInt(i)] = i;
        return new ImmutablePair<>(indices, r[1].data().asFloat());
    }
    /**
     * 一维排序
     * @param x 一维数组
     * @return (结果索引, 排序结果)
     */
    public static ImmutablePair<int[], float[]> ArgSortWithoutRank(float[] x){
        INDArray X = Nd4j.create(x);
        INDArray[] r = Nd4j.sortWithIndices(X, 1, false);
        int[] indices = new int[X.length()];
        for(int i = 0; i < r[0].length(); i++)
            indices[i] = (int)r[0].getInt(i);
        return new ImmutablePair<>(indices, r[1].data().asFloat());
    }

    /**
     * 二维排序
     * @param x 排序的二维数组
     * @return (排名, 排序结果)
     */
    public static ImmutablePair<int[][], float[][]>
    ArgSort(float[][] x, int dim, boolean ascend){
        INDArray X = Nd4j.create(x);
        return ArgSort(X, dim, ascend);
    }

    public static ImmutablePair<int[][], float[][]>
    ArgSort(INDArray X, int dim, boolean ascend){
        INDArray[] r = Nd4j.sortWithIndices(X, dim, ascend);
        int rows = X.rows(), cols = X.columns();
        int[][] indices = new int[rows][cols];
        float[][] sorted = new float[rows][cols];

        if(dim == 0){
        for(int j = 0; j < cols; j++){
            for(int i = 0; i < rows; i++){
                indices[r[0].getInt(i, j)][j] = i;
                sorted[i][j] = r[1].getFloat(i, j);
            }
        }}else if(dim == 1){
        for(int i = 0; i < rows; i++){
            for(int  j= 0; j < cols; j++){
                indices[i][r[0].getInt(i, j)] = j;
                sorted[i][j] = r[1].getFloat(i, j);
            }
        }}
        
        return new ImmutablePair<>(indices, sorted);
    }

    /**
     * 获取x每列的最大值的索引
     * @param x NxM 矩阵
     * @return Nx1 最大值索引
     */
    public static int[] ArgMax(float[][] x){
        INDArray X = Nd4j.create(x);
        int row = X.rows();
        INDArray max_idx = Nd4j.argMax(X, 1);

        int[] indices = new int[row];
        for(int i = 0; i < max_idx.rows(); i++)
            indices[i] = max_idx.getInt(i);
        
        return indices;
    }

    /**
     * 拟合直线(X, Y) 必须一样大小
     * @param x N个x
     * @param y N个y
     * @return
     */
    public static float[] LinearRegression(float[] x, float[] y){
        if(x.length <= 1 || y.length <= 1)
            return new float[]{0.0f, 0.0f};
        
        SimpleRegression R = new SimpleRegression();
        float[] params = new float[2];
        for(int i = 0; i < x.length; i++)
            R.addData(x[i], y[i]);
        params[0] = (float)R.getSlope();
        params[1] = (float)R.getIntercept();

        return params;
    }

    /**
     * 计算对对索引的回归曲线
     * @param xx NxM 矩阵
     * @return Mx2的直线参数
     */
    public static float[][] LinearRegression(float[][] xx){
        INDArray X = Nd4j.create(xx);
        int row = X.rows(), col = X.columns();
        float[][] params = new float[col][];
        float[] idx = Range(row);

        for(int i = 0; i < col; i++){
            params[i] = LinearRegression(
                idx,
                X.getColumn(i).dup().data().asFloat());
        }

        return params;
    }

    public static float[] Range(int num){
        float[] result = new float[num];
        for(int i = 0; i < num; i++)
            result[i] = i+1;
        return result;
    }
}
