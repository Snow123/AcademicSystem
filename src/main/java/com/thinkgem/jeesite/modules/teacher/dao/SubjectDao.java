/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.dao;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.teacher.entity.Subject;

/**
 * @author songhao
 *
 */
@MyBatisDao
public interface SubjectDao extends CrudDao<Subject> {

	int getKnowledgeBySubjectId(int subjectId);

	int getScoreBySubjectId(int subjectId);

}
