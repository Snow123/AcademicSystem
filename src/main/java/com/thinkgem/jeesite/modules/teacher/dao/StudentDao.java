/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.sys.entity.User;
import com.thinkgem.jeesite.modules.teacher.entity.Class;
import com.thinkgem.jeesite.modules.teacher.entity.Student;
import com.thinkgem.jeesite.modules.teacher.entity.StudentScore;

/**
 * @author paul
 *
 */
@MyBatisDao
public interface StudentDao extends CrudDao<Student> {

	List<StudentScore> getClass(String firstYear);

	void addUser(User user);

	String getRoleId(String roleName);

	void addUserRole(String id, String roleId);

	void deleteStudentUser(User user);

	void deleteStudentByAccount(Student student);

	void deleteStudentScore(Student student);

	void deleteClass(@Param("schoolCode") String schoolCode, @Param("firstYear") String firstYear);

	void addClass(Class info);

}
