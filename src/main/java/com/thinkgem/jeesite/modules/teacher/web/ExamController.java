/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.web;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.utils.StringUtils;
import com.thinkgem.jeesite.common.web.BaseController;
import com.thinkgem.jeesite.modules.sys.entity.Dict;
import com.thinkgem.jeesite.modules.teacher.entity.Exam;
import com.thinkgem.jeesite.modules.teacher.service.ExamService;

/**
 * @author paul
 *
 */
@Controller
@RequestMapping(value = "${adminPath}/teacher/exam")
public class ExamController extends BaseController {

	@Autowired
	private ExamService examService;
	
	@ModelAttribute
	public Exam get(@RequestParam(required=false) String id) {
		if (StringUtils.isNotBlank(id)){
			return examService.getExam(id);
		}else{
			return new Exam();
		}
	}
	
	@RequiresPermissions("teacher:exam:view")
	@RequestMapping(value = {"list", ""})
	public String list(Exam exam, HttpServletRequest request, HttpServletResponse response, Model model) {
		Page<Exam> page = examService.findExam(new Page<Exam>(request, response), exam);
        model.addAttribute("page", page);
		return "modules/teacher/examManage";
	}
	
	@RequiresPermissions("teacher:exam:view")
	@RequestMapping(value = "form")
	public String form(Exam exam, Model model) {
		model.addAttribute("exam", exam);
		return "modules/teacher/examForm";
	}
	
	@RequiresPermissions("teacher:exam:edit")
	@RequestMapping(value = "delete")
	public String delete(Exam exam, RedirectAttributes redirectAttributes) {
		int examScoreCount = examService.getExamScoreCountById(exam);
		int examQuestionCount = examService.getExamQuestionCountById(exam);
		if ((examScoreCount + examQuestionCount) == 0) {
			examService.deleteExam(exam);
			addMessage(redirectAttributes, "删除考试成功");
		} else if (examScoreCount == 0 && examQuestionCount != 0) {
			addMessage(redirectAttributes, "删除失败：已维护试卷的考试不可删除！");
		} else if (examScoreCount != 0 && examQuestionCount == 0) {
			addMessage(redirectAttributes, "删除失败：已维护成绩的考试不可删除！");
		} else {
			addMessage(redirectAttributes, "删除失败：已维护试卷或成绩的不可删除！");
		}
		return "redirect:" + adminPath + "/teacher/exam/list?repage";
	}
	
	@RequiresPermissions("teacher:exam:edit")
	@RequestMapping(value = "save")
	public String save(Exam exam, HttpServletRequest request, Model model, RedirectAttributes redirectAttributes) {
		// 保存考试信息
		exam.setGradeCode(exam.getTermCode().substring(0, 2));
		if (exam.getExamId() != null) {
			examService.save(exam);
		} else {
			examService.insert(exam);
		}
		addMessage(redirectAttributes, "保存成功");
		return "redirect:" + adminPath + "/teacher/exam/list?repage";
	}
	
	public List<Dict> getTermList(){
		return examService.getTermList();
	}
	
	public List<Dict> getSubjectList(){
		return examService.getSubjectList();
	}
	
}
