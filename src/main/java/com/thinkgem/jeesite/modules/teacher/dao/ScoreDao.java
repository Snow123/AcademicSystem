/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.dao;

import java.util.List;

import org.apache.ibatis.annotations.Param;

import com.thinkgem.jeesite.common.persistence.CrudDao;
import com.thinkgem.jeesite.common.persistence.annotation.MyBatisDao;
import com.thinkgem.jeesite.modules.teacher.entity.Exam;
import com.thinkgem.jeesite.modules.teacher.entity.ScoreSub;
import com.thinkgem.jeesite.modules.teacher.entity.StudentScore;
import com.thinkgem.jeesite.modules.teacher.entity.Subject;

/**
 * @author songhao
 *
 */
@MyBatisDao
public interface ScoreDao extends CrudDao<ScoreSub> {

	List<String> getSubjects(@Param("schoolCode") String schoolCode, @Param("termCode") String termCode);

	List<String> getQuestionCodes(String examId, String subjectId);

	List<Subject> getSubjectLists(@Param("schoolCode") String schoolCode, @Param("termCode") String termCode);

	void insertSubjectScore(ScoreSub score);

	String getSubjectId(@Param("schoolCode") String schoolCode, @Param("termCode") String termCode, @Param("subjectName") String subjectName);

	String getQuestionId(String examId, String subjectId, String questionCode);

	String getScoreId(String examId, String subjectId, String studentAccount);

	void insertQuestionScore(ScoreSub score);

	void deleteSubjectScore(ScoreSub score);

	void deleteQuestionScore(ScoreSub score);

	List<StudentScore> getScoreList(StudentScore studentScore);

	String getExamIdByName(String examName);

	List<Exam> getExamName(@Param("firstYear") String firstYear, @Param("termCode") String termCode);

}
