/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.entity;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.thinkgem.jeesite.common.persistence.DataEntity;
import com.thinkgem.jeesite.common.utils.excel.annotation.ExcelField;

/**
 * @author paul
 *
 */
public class Student extends DataEntity<Student> {

	private static final long serialVersionUID = 1L;
	
	private String ids; // 学生ID集合
	private String studentId; // 学生ID
	private String studentAccount; // 账号
	private String studentCode; // 学籍号
	private String firstYear; // 入学年份
	private String classId; // 行政班级
	private String studentNo; // 班内学号
	private String name; // 姓名
	private String sex; // 性别
	private String isValid; // 当前是否有效
	
	public String getIds() {
		return ids;
	}
	
	public void setIds(String ids) {
		this.ids = ids;
	}
	
	public String getStudentId() {
		return studentId;
	}
	
	public void setStudentId(String studentId) {
		this.studentId = studentId;
	}
	
	@JsonIgnore
	@NotNull(message="账号不能为空")
	@ExcelField(title="账号", align=2, sort=30)
	@Length(min=1, max=9, message="账号长度必须介于 1 和 9 之间")
	public String getStudentAccount() {
		return studentAccount;
	}
	
	public void setStudentAccount(String studentAccount) {
		this.studentAccount = studentAccount;
	}
	
	public String getStudentCode() {
		return studentCode;
	}
	
	public void setStudentCode(String studentCode) {
		this.studentCode = studentCode;
	}
	
	@JsonIgnore
	@NotNull(message="年级不能为空")
	@ExcelField(title="年级", align=2, sort=20)
	@Length(min=1, max=4, message="年级长度必须为 4 之间")
	public String getFirstYear() {
		return firstYear;
	}
	
	public void setFirstYear(String firstYear) {
		this.firstYear = firstYear;
	}

	@JsonIgnore
	@NotNull(message="班级不能为空")
	@ExcelField(title="班级", align=2, sort=25)
	@Length(min=1, max=2, message="班级长度必须介于 1 和2 之间")
	public String getClassId() {
		return classId;
	}
	
	public void setClassId(String classId) {
		this.classId = classId;
	}

	public String getStudentNo() {
		return studentNo;
	}
	
	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}
	
	@JsonIgnore
	@NotNull(message="姓名不能为空")
	@ExcelField(title="姓名", align=2, sort=35)
	@Length(min=1, max=8, message="姓名长度必须介于 1 和 8 之间")
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@JsonIgnore
	@NotNull(message="性别不能为空")
	@ExcelField(title="性别", align=2, sort=40)
	@Length(min=1, max=1, message="性别长度必须为 1")
	public String getSex() {
		return sex;
	}
	
	public void setSex(String sex) {
		this.sex = sex;
	}
	
	public String getIsValid() {
		return isValid;
	}
	
	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}
	
}
