/**
 * 
 */
package com.thinkgem.jeesite.modules.teacher.service;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletResponse;

import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFRichTextString;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.thinkgem.jeesite.common.persistence.Page;
import com.thinkgem.jeesite.common.service.BaseService;
import com.thinkgem.jeesite.common.utils.Encodes;
import com.thinkgem.jeesite.modules.teacher.dao.ScoreDao;
import com.thinkgem.jeesite.modules.teacher.entity.Exam;
import com.thinkgem.jeesite.modules.teacher.entity.ScoreSub;
import com.thinkgem.jeesite.modules.teacher.entity.StudentScore;
import com.thinkgem.jeesite.modules.teacher.entity.Subject;

/**
 * @author songhao
 *
 */
@Service
@Transactional(readOnly = true)
public class ScoreService extends BaseService {

	@Autowired
	private ScoreDao scoreDao;

	public Page<StudentScore> getStudentScore(Page<StudentScore> page, StudentScore studentScore) {
		// 生成数据权限过滤条件（dsf为dataScopeFilter的简写，在xml中使用 ${sqlMap.dsf}调用权限SQL）
		studentScore.getSqlMap().put("dsf", dataScopeFilter(studentScore.getCurrentUser(), "o", "a"));
		// 设置分页参数
		studentScore.setPage(page);
		// 执行分页查询
		page.setList(scoreDao.getScoreList(studentScore));
		return page;
	}

	public void importSubjectScoreTemplate(String termCode, HttpServletResponse response) throws Exception {
		termCode = "";
		// 从session中取得schoolCode
		String schoolCode = "";
		// 取得当前在用科目列表
		
		List<String> subjectList = scoreDao.getSubjects(schoolCode, termCode);
		
		Workbook wb = new XSSFWorkbook();
		Sheet sheet1 = (Sheet) wb.createSheet("sheet1");
		HSSFWorkbook workbook = new HSSFWorkbook();
		Font ztFont = wb.createFont();
		ztFont.setColor(Font.COLOR_NORMAL);
		ztFont.setFontName("宋体");
		XSSFCellStyle ztStyle = (XSSFCellStyle) wb.createCellStyle();
		ztStyle.setWrapText(true);
		ztStyle.setFont(ztFont);
		HSSFDataFormat format = workbook.createDataFormat();
		ztStyle.setDataFormat(format.getFormat("0"));
		ztStyle.setVerticalAlignment(XSSFCellStyle.VERTICAL_CENTER);
		try {
			CellRangeAddress region = new CellRangeAddress(0, 0, 0, subjectList.size()+1);
			sheet1.addMergedRegion(region);
			Cell cell = null;
			Row row = (Row) sheet1.createRow(0);
			row.setHeightInPoints(6*sheet1.getDefaultRowHeightInPoints());
			cell = row.createCell(0);
			cell.setCellValue(new XSSFRichTextString("填写说明：\r\n" + 
					"（1）学生账号9位，前3位为学校编码，由客服人员分配，不知道请咨询管理员；\r\n" + 
					"（2）学生姓名为2到4个汉字，中间不带空格。\r\n" + 
					"（3）语数外最高分150，其它科目最高分100,成绩可为整数，或包含1位小数；\r\n" + 
					"（4）导入时，请删除填写说明栏所在行，保留表头所在行，并保持模板中的科目名称不变；\r\n" + 
					"（5）请不要导入缺考和单科缺考学生成绩。"));
			cell.setCellStyle(ztStyle);
			Row row0 = (Row) sheet1.createRow(1);
			cell = row0.createCell(0);
			cell.setCellValue("学生账号");
			cell.setCellStyle(ztStyle);
			cell = row0.createCell(1);
			cell.setCellValue("姓名");
			cell.setCellStyle(ztStyle);
			for (int i = 0; i < subjectList.size(); i++) {
				cell = row0.createCell(i+2);
				cell.setCellValue(subjectList.get(i));
				cell.setCellStyle(ztStyle);
			}

			String filename = "科目成绩模板.xlsx";
			response.reset();
			response.setContentType("application/octet-stream; charset=utf-8");
			response.setHeader("Content-disposition", "attachment;filename=" + Encodes.urlEncode(filename));
			OutputStream ouputStream = response.getOutputStream();
			wb.write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (Exception e) {
			e.printStackTrace();
			throw e;
		}
	}

	public void importQuestionScoreTemplate(String termCode, String examId, HttpServletResponse response) throws Exception {
		termCode = "";
		// 从session中取得schoolCode
		String schoolCode = "";
		// 取得当前在用科目列表
		List<String> subjectList = scoreDao.getSubjects(schoolCode, termCode);
		// 取得当前在用科目列表并将科目名称和科目ID转化为K,V
		List<Subject> subList = scoreDao.getSubjectLists(schoolCode, termCode);
		Map<String, String> map = new HashMap<String, String>();
		for (Subject subject : subList) {
			map.put(subject.getSubjectName(), subject.getSubjectId()+"");
		}
		try {
			Workbook wb = new XSSFWorkbook();
			HSSFWorkbook workbook = new HSSFWorkbook();
			Font ztFont = wb.createFont();
			ztFont.setColor(Font.COLOR_NORMAL);
			XSSFCellStyle ztStyle = (XSSFCellStyle) wb.createCellStyle();
			ztStyle.setFont(ztFont);
			HSSFDataFormat format = workbook.createDataFormat();
			ztStyle.setDataFormat(format.getFormat("0"));
			
			for (int i = 0; i < subjectList.size(); i++) {
				Sheet sheet = (Sheet) wb.createSheet(subjectList.get(i));
				List<String> questionList = scoreDao.getQuestionCodes(examId, map.get(subjectList.get(i)));
				Row row0 = (Row) sheet.createRow(0);
				Cell cell = null;
				cell = row0.createCell(0);
				cell.setCellValue("学生账号");
				cell.setCellStyle(ztStyle);
				cell = row0.createCell(1);
				cell.setCellValue("姓名");
				cell.setCellStyle(ztStyle);
				for (int j = 0; j < questionList.size(); j++) {
					cell = row0.createCell(j+2);
					cell.setCellValue(questionList.get(j));
					cell.setCellStyle(ztStyle);
				}
			}
			String filename = "科目试题成绩模板.xlsx";
			response.reset();
			response.setContentType("application/octet-stream; charset=utf-8");
			response.setHeader("Content-disposition", "attachment;filename=" + Encodes.urlEncode(filename));
			OutputStream ouputStream = response.getOutputStream();
			wb.write(ouputStream);
			ouputStream.flush();
			ouputStream.close();
		} catch (IOException e) {
			e.printStackTrace();
			throw e;
		}
	}

	public List<Subject> getSubjectLists(String schoolCode, String termCode) {
		return scoreDao.getSubjectLists(schoolCode, termCode);
	}

	public void insertSubjectScore(ScoreSub score) {
		scoreDao.insertSubjectScore(score);
	}

	public void insertQuestionScore(ScoreSub score) {
		scoreDao.insertQuestionScore(score);
	}

	public void deleteSubjectScore(ScoreSub score) {
		scoreDao.deleteSubjectScore(score);
	}

	public void deleteQuestionScore(ScoreSub score) {
		scoreDao.deleteQuestionScore(score);
	}

	public String getExamIdByName(String examName) {
		return scoreDao.getExamIdByName(examName);
	}

	public List<Exam> getExamName(String firstYear, String termCode) {
		return scoreDao.getExamName(firstYear, termCode);
	}
	
}
