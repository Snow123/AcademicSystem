<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>科目维护</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			
		});
		function page(n,s){
			if(n) $("#pageNo").val(n);
			if(s) $("#pageSize").val(s);
			if ($("#isValid").prop('checked') && $("#isNotValid").prop('checked')) {
				$("#isValid").attr("checked",false);
				$("#isNotValid").attr("checked",false);
			}
			$("#searchForm").attr("action","${ctx}/teacher/subject/list");
			$("#searchForm").submit();
	    	return false;
	    }
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/teacher/subject/list">科目列表</a></li>
		<shiro:hasPermission name="teacher:subject:edit"><li><a href="${ctx}/teacher/subject/form">科目添加</a></li></shiro:hasPermission>
	</ul>
	<form:form id="searchForm" modelAttribute="subject" action="${ctx}/teacher/subject/list" method="post" class="breadcrumb form-search ">
		<input id="pageNo" name="pageNo" type="hidden" value="${page.pageNo}"/>
		<input id="pageSize" name="pageSize" type="hidden" value="${page.pageSize}"/>
		<sys:tableSort id="orderBy" name="orderBy" value="${page.orderBy}" callback="page();"/>
		
 		<ul class="ul-form">
 			<li>
 				<label class="control-label">是否在用:</label>
 				<input type="checkbox" name="isValid" id="isValid" value="1" />是 
				<input type="checkbox" name="isValid" id="isNotValid" value="0" />否
			</li>
			<li class="btns">
				<input id="btnSubmit" class="btn btn-primary" type="submit" value="查询" onclick="return page();"/>
				<a href="${ctx}/teacher/subject/form"><input class="btn btn-primary" type="button" value="新增"/></a>
			</li>
			<li class="clearfix"></li>
		</ul>
		
	</form:form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead><tr><th>科目分类</th><th>科目名称</th><th>满分</th><th>本校优势学科</th><th>是否在用</th><shiro:hasPermission name="teacher:subject:edit"><th>操作</th></shiro:hasPermission></tr></thead>
		<tbody>
		<c:forEach items="${page.list}" var="subject">
			<tr>
				<td>${fns:getSubjectClass(subject.subjectClass)}</td>
				<td>${subject.subjectName}</td>
				<td>${subject.fullScore}</td>
				<td>${fns:getIsAdvanced(subject.isAdvanced)}</td>
				<td>${fns:getIsValid(subject.isValid)}</td>
				<shiro:hasPermission name="teacher:subject:edit"><td>
					<a href="${ctx}/teacher/subject/delete?subjectId=${subject.subjectId}" onclick="return confirmx('确认要删除此科目吗？', this.href)">删除</a>
				</td></shiro:hasPermission>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>