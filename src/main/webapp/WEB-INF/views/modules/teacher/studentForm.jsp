<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>学生管理</title>
	<meta name="decorator" content="default"/>
	<script type="text/javascript">
		$(document).ready(function() {
			$("#inputForm").validate({
 				rules: {
					classId: {digits:true}
				},
				messages: {
					classId: {digits:"必须为数字"}
				}, 
				submitHandler: function(form){
					loading('正在提交，请稍等...');
					form.submit();
				},
				errorContainer: "#messageBox",
				errorPlacement: function(error, element) {
					$("#messageBox").text("输入有误，请先更正。");
 					if (element.is(":checkbox")||element.is(":radio")||element.parent().is(".input-append")){
						error.appendTo(element.parent().parent());
					} else {
						error.insertAfter(element);
					}
				}
			});
		});
	</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li><a href="${ctx}/teacher/student/list">学生列表</a></li>
		<li class="active"><a href="${ctx}/teacher/student/form?id=${student.studentId}">学生<shiro:hasPermission name="teacher:student:edit">${not empty student.studentId?'修改':'添加'}</shiro:hasPermission><shiro:lacksPermission name="teacher:student:edit">查看</shiro:lacksPermission></a></li>
	</ul><br/>
	<form:form id="inputForm" modelAttribute="student" action="${ctx}/teacher/student/save" method="post" class="form-horizontal">
		<form:hidden path="id"/>
		<sys:message content="${message}"/>
		<div class="control-group">
			<label class="control-label">账号:</label>
			<div class="controls">
				<form:input path="studentAccount" htmlEscape="false" maxlength="9" class="required"/>
				<span class="help-inline"><font color="red">*</font> </span>
				<span class="help-inline">最大长度为9位</span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">入学年份:</label>
			<div class="controls">
				<select id="firstYear" name="firstYear" class="input-medium" value="${student.firstYear}" class="required">
					<option value="" selected="selected"></option>
					<option value="2016" <c:if test="${student.firstYear=='2016'}">selected</c:if> >2016</option>
					<option value="2017" <c:if test="${student.firstYear=='2017'}">selected</c:if> >2017</option>
					<option value="2018" <c:if test="${student.firstYear=='2018'}">selected</c:if> >2018</option>
					<option value="2019" <c:if test="${student.firstYear=='2019'}">selected</c:if> >2019</option>
					<option value="2020" <c:if test="${student.firstYear=='2020'}">selected</c:if> >2020</option>
					<option value="2021" <c:if test="${student.firstYear=='2021'}">selected</c:if> >2021</option>
					<option value="2022" <c:if test="${student.firstYear=='2022'}">selected</c:if> >2022</option>
					<option value="2023" <c:if test="${student.firstYear=='2023'}">selected</c:if> >2023</option>
					<option value="2024" <c:if test="${student.firstYear=='2024'}">selected</c:if> >2024</option>
					<option value="2025" <c:if test="${student.firstYear=='2025'}">selected</c:if> >2025</option>
				</select>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">班级:</label>
			<div class="controls">
				<form:input path="classId" name="classId" htmlEscape="false" maxlength="2" pattern="(\d{1,2})$" class="required"/>
				<span class="help-inline"><font color="red">*</font> </span>
				<span class="help-inline">必须为数字类型</span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">班内学号:</label>
			<div class="controls">
				<form:input path="studentNo" htmlEscape="false" maxlength="2" minlength="1" class="required"/>
				<span class="help-inline"><font color="red">*</font> </span>
				<span class="help-inline">最大长度为2位</span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">姓名:</label>
			<div class="controls">
				<form:input path="name" htmlEscape="false" maxlength="16" class="required"/>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="control-group">
			<label class="control-label">性别:</label>
			<div class="controls">
				<c:if test="${student.sex=='1'}">
	 				<input type="radio" name="sex" value="1" checked="checked" />男
					<input type="radio" name="sex" value="0"  />女
				</c:if>
				<c:if test="${student.sex=='0'}">
	 				<input type="radio" name="sex" value="1" />男
					<input type="radio" name="sex" value="0" checked="checked" />女
				</c:if>
				<span class="help-inline"><font color="red">*</font> </span>
			</div>
		</div>
		<div class="form-actions">
			<shiro:hasPermission name="teacher:student:edit"><input id="btnSubmit" class="btn btn-primary" type="submit" value="保 存"/>&nbsp;</shiro:hasPermission>
			<input id="btnCancel" class="btn" type="button" value="返 回" onclick="history.go(-1)"/>
		</div>
	</form:form>
</body>
</html>