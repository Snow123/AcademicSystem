<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
<title>组合选择预测</title>
<meta name="decorator" content="default"/>
<script type="text/javascript">
$(document).ready(function() {
	var url = "echart6";
	$.ajax({
		url:url,
		dataType:"json",
		type:"POST",
		success:function(data){
			
			var sub = data[0].adviceSub;
			var subHtml = "";
			$.each(sub,function(key,value){
				subHtml += value +"、";
			})
			$("#count").html(subHtml.substring(0,subHtml.length-1));
			var count = data[0].count
			var html = "";
			$.each(count,function(index,item){
				var index1 = index+1;
				html += "<th>"+index1+"班</th>"
			})
			$("#contentTable tr").append(html);
			
			var content = "";
			$.each(data,function(index,item){
				content = "<tr>";
				content+= "<td>"+item.subjectName+"</td>";
				content+= "<td>"+item.peoplePred+"</td>";
				content+= "<td>"+item.classPercent+"</td>";
				$.each(item.count,function(index,value){
					content	 += "<td>"+value+"</td>"
				})
				content += "</tr>";
				$("#contentTable").append(content);
			})
		},
		error:function(){
			
		}
	})
})

</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/adviceStu/controller/subComSelect">组合选择预测</a></li>
	</ul>
	<div  style = "float:left;position:relative;left:3%;bottom:3%">本校优秀学科：<span id = "count"></span>
	</div>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<tr>
			<th>组合名称</th>
			<th>预计选择人数</th>
			<th>年级占比</th>
		</tr>
	</table>
</body>
</html>