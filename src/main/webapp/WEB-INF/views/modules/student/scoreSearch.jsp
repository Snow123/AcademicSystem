<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
	<title>成绩查询</title>
	<meta name="decorator" content="default"/>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/student/scoreSearch/scoreSearch">成绩查询</a></li>
	</ul>
	<form id="searchForm" modelAttribute="search" action="${ctx}/student/scoreSearch/scoreSearch" method="post" class="breadcrumb form-search ">
		<ul class="ul-form">
			<li>
				<label>学期：</label>
				<select name="termCode" onchange="termChange(this.options[this.options.selectedIndex].value)">
					<c:forEach items ="${termList}" var = "term">
						<option value="${term.termCode}" >${term.termName}</option>
					</c:forEach>
				<select>
			</li>
			<script type="text/javascript">
				function termChange(v){
					$.ajax({
				        type : "post",
				        async : false,
				        url : "getExam",
				        data : {
				            'termCode' : v
				        },
				        dataType : "json",
				        success : function(msg) {
				        	console.log(msg);
				            $("#examName").empty();
				            if (msg.length > 0) {
				                for (var i = 0; i < msg.length; i++) {
				                        var examId = msg[i].examId;
				                        var examName = msg[i].examName;
				                        var $option = $("<option>").attr({
				                            "value" : examId
				                        }).text(examName);
				                        $("#examName").append($option);
				                }
				            }
				            $("#examName").change();
				        },
				        error : function(json) {
				            $.jBox.alert("网络异常！");
				        }
				    });
				}
			</script>
			<li>
				<label>考试：</label>
				<select name="examId" id = "examName">
					<c:forEach items ="${examList}" var = "exam">
						<option value="${exam.examId}" >${exam.examName}</option>
					</c:forEach>
				<select>
			</li>
			<li class="btns"><input id="btnSubmit" class="btn btn-primary" type="submit" value="查询" /> </li>
			<li style="float:right;"><label>总成绩：</label><span>${allScore}</span> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</li>
			<li class="clearfix"></li>
		</ul>
	</form>
	<sys:message content="${message}"/>
	<table id="contentTable" class="table table-striped table-bordered table-condensed">
		<thead>
			<tr>
				<th>试题编码</th>
				<th>知识点</th>
				<th  >知识点分类</th>
				<th >满分</th>
				<th>得分</th>
			</tr>
		</thead>
		<tbody>
		<c:forEach items="${scoreList}" var="score">
			<tr>
				<td>${score.subjectName}</td>
				<td>${score.subjectScore}</td>
				<td>${score.classStandard}</td>
				<td>${score.classLevel}</td>
				<td>${score.gradeStandard}</td>
 				<td>${score.gradeLevel}</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<div class="pagination">${page}</div>
</body>
</html>