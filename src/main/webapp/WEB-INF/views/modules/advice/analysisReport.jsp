<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
<title>学业分析结果报告</title>
<meta name="decorator" content="default"/>
<script src="${ctxStatic}/echart/echarts.js" type="text/javascript"></script>
<style>


</style>
<link href="${ctxStatic}/report-css/style.css" rel="stylesheet" type="text/css">
<style>
	.overflow_div{
		width:1280px;
		overflow-x:auto;
		margin:0 auto;
		margin-bottom:20px;
		}	
</style>
<script type="text/javascript">
	$(function(){
		$.ajax({
			url:"echart2",
			dataType:"json",
			type:"POST",
			success:function(data){
				var x = new Array();
				var y = new Array();
				$("#comLevel").html(data.best.substring(0,data.best.length-1))

				$.each(data,function(key,value){
					if(key!= 'count' && key != 'best'){
						x.push(key);
						y.push(value);
					}
				});
				createEchart(x,y,data.count)
			},
			error:function(){
			}
		});
		$.ajax({
			url:"echart4",
			dataType:"json",
			type:"POST",
			success:function(data){
				var x = new Array();
				var y = new Array();
				createEchart2(data.x,data.y,data.count);
                $("#comSteady").html(data.best.substring(0,data.best.length-1))
			},
			error:function(){
				
			}
		});
		
		$.ajax({
			url:"echart5",
			dataType:"json",
			type:"POST",
			success:function(data){
				$("#sub").html(data.best.substring(0,data.best.length-1)+"           ");
				var i = 0;
				$.each(data,function(key,value){
					if(key == "物理"){
						createEchart3("com3",data.scoreTime,value.score,key,value.funl,value.start,value.end);
					}
					if(key == "化学"){
						createEchart3("com4",data.scoreTime,value.score,key,value.funl,value.start,value.end);
					}
					if(key == "生物"){
						createEchart3("com5",data.scoreTime,value.score,key,value.funl,value.start,value.end);
					}
					if(key == "历史"){
						createEchart3("com6",data.scoreTime,value.score,key,value.funl,value.start,value.end);
					}
					if(key == "地理"){
						createEchart3("com7",data.scoreTime,value.score,key,value.funl,value.start,value.end);
					}
					if(key == "政治"){
						createEchart3("com8",data.scoreTime,value.score,key,value.funl,value.start,value.end);
					}
				});
			},
			error:function(){
				
			}
		});
		$.ajax({
			url:"echart6",
			dataType:"json",
			type:"POST",
			success:function(data){
				
				var sub = data[0].adviceSub;
				var subHtml = "";
				if(!sub == undefined ){
					$.each(sub,function(key,value){
						subHtml += value +"、";
					});
					$("#count").html(subHtml.substring(0,subHtml.length-1));
				}
				var content = "";
				$.each(data,function(index,item){
					content = "<tr>";
					content+= "<td>"+item.subjectName+"</td>";
					content+= "<td>"+item.peoplePred+"</td>";
					content+= "<td>"+item.classPercent+"</td>";
					content+= "<td>"+item.zorder+"</td>";
					content += "</tr>";
					$("#contentTable").append(content);
				})
			},
			error:function(){
				
			}
		})
	});
	function createEchart(x,y,count){
		
		var yy = [];
		for(var i = 0;i<y.length;i++){
			yy[i] = count - y[i];
		}
		console.log(x)
		console.log(y);
		console.log(yy)
		var dom = document.getElementById("com1");
		var myChart = echarts.init(dom);
		var app = {};
		option = null;
		option = {
		    xAxis: {
		        type: 'category',
		        data: x,
		        name:'科目组合',
		        interval: 0,
		        maxInterval:0
		    },
		    yAxis: {
		        type: 'value',
		        name:'学年名次',
		        axisLabel : {
	                formatter: function(v){
	                    return count-v +1;
	                }
	            }
		    },
		    series: [{
		        data: yy,
		        type: 'bar',
		        label:{ 
		            normal:{ 
			            show: true, 
			            position: 'top',
			            formatter: function(params) { 
			            	for (var i = 0, l = option.xAxis.data.length; i < l; i++) {  
			            		if (option.xAxis.data[i] == params.name) {  
			            			 var val1 = count - params.value +1;
			            			 return val1;  
			            		}
			            	}
			            }
		            } 
		         }
		    }]
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}
	function createEchart2(x,y,count){
		var dom = document.getElementById("com2");
		var myChart = echarts.init(dom);
		var app = {};
		option = null;
		option = {
		    xAxis: {
		        type: 'category',
		        data: x,
		        name:'科目组合',
		        interval: 0,
		        maxInterval:0
		    },
		    yAxis: {
		        type: 'value',
		        name:'排名均值'
		    },
		    series: [{
		        data: y,
		        type: 'bar',
		        label:{ 
		            normal:{ 
			            show: true, 
			            position: 'top'
		            } 
		         }
		    }]
		};
		;
		if (option && typeof option === "object") {
		    myChart.setOption(option, true);
		}
	}


    function createEchart3(id,x,y,title,zhi,start,end){
        var data = [];
        var startPoint = [0,start];
        var endPoint = [x[x.length-1],end];
        for(var i =0;i<x.length;i++){
            var xx = x[i];
            var yy = y[i];
            data[i] = [xx,yy];

        }
        var dom = document.getElementById(id);
        var myChart = echarts.init(dom);
        option = {
            title: {
                text: title,
                x: 'center',
                y: 0
            },
            xAxis:  {
                type:'category',
                name:'历次考试',
                nameTextStyle:{
                    fontSize:8
                },
                data:x,
                interval: 0,
                axisLabel:{
                    show:true
                },
                axisLine:{
                    onZero:false
                }
            },
            yAxis: {name:'标准分', min: -5,max:5,grid:0,interval:1 },
            series: [
                {
                    name: title,
                    type: 'scatter',
                    data: data,
                    markLine: {
                        animation: false,
                        label: {
                            normal: {
                                formatter: zhi,
                                textStyle: {
                                    align: 'right'
                                }
                            }
                        },
                        lineStyle: {
                            normal: {
                                type: 'solid'
                            }
                        },
                        tooltip: {
                            formatter: zhi
                        },
                        data: [[{
                            coord: startPoint,
                            symbol: 'none'
                        }, {
                            coord: endPoint,
                            symbol: 'none'
                        }]]
                    }
                }
            ]
        };
        if (option && typeof option === "object") {
            myChart.setOption(option, true);
        }
    }


</script>
</head>
<body>
	<div class="wrap">
	  <div class="index_a">
			<img src="${ctxStatic}/report-css/11.jpg" width="670" height="249" alt=""/>
	   		 <ul  items ="${info}">
	        	<li><span class="class_tx">账号：</span><span class="bor_bottom" id="account">${info.studentAccount}</span></li>
	            <li><span class="class_tx">姓名：</span><span class="bor_bottom" id ="name1">${info.name}</span></li>
	            <li><span class="class_tx">年级：</span><span class="bor_bottom" id = "firstYear">${info.firstYear}</span></li>
	            <li><span class="class_tx">班级：</span><span class="bor_bottom" id = "classCode">${info.className}</span></li>
	            <li><span class="class_tx">测试时间：</span><span class="bor_bottom" id = "test">${info.scoreDate}</span></li>
	        </ul>
	  </div>
	  <div class="index_b">
	   	<ul>
	        	<li><span>东北师范大学青少年生涯教育研究中心</span></li>
	            <li><span>新基础教育研究院</span></li>
	            <li><span> www.shengyazhidao.com</span></li>
	        </ul>
	    </div>
		<p  items ="${info}">亲爱的<span id = "name">${info.name}</span>同学：</p>
		<p>您好！</p>
	  <p style="text-indent:2em;">新高考最核心的改变是赋予学生更多的选择权，促进每位学生的个性发展。它将基础教育与高等教育的连接将从单一的分数对接走向多元对接。具体说来包括三个方面：一是学习兴趣的连结：高中生必然是基于自身兴趣来选择选修课程。
	二是专业学习基础的双向连结：高校将根据大学学习相关专业必须具备的高中相关学科知识基础分专业地确定高中生的选考科目。三是综合素质的双向连结：将体现学生综合素质的标志性成果纳入高校招生依据，考察的是高中学生的综合素质与高校专业学习的相关性，突破了过去高校招生只看冷冰冰的分，忽视活生生的人的局面。
	所以，对于高中学生来说，新高考一定是有好处的。你将可以选择自己的优势学科进行重点突破，而不必被迫学习自己特别不喜欢的学科。而对于学习来说，兴趣无疑是最重要的一个指向标。没有兴趣就没有学习，就不能释放学习内驱力，就不能发展自己的天赋与个性。
	而高考毕竟是一场优胜劣汰的竞争。我们都希望可以笑傲考场，那只考虑兴趣就是远远不够的。我们还需要考虑很多重要的因素，比如学科成绩、学科发展潜力、未来的理想职业、目标专业、高校专业限考要求等等。下面我们按重要性为大家逐一解释以上影响因素。</p>
		<p><strong>第一，学科优势。</strong>这个因素是最核心的影响因素，它包括了当前学科成绩和学科潜力两项。优势学科指的是兼具能力与兴趣的学科。如果兴趣浓厚，但未取得领先的学科可视为潜能学科，未来可能会有极大的突破，但结果并不确定。如果当前成绩优秀，但自己认定并无浓厚兴趣的学科可视为退路的学科，在无优势学科的前提下可以选择退路学科。这类学科一般来说后劲不够，最终可能并不能取得长足的发展，当然也不排除可以在以后的学习中培养出兴趣。将学科优势进一步分解，可以分为学科兴趣、学科能力、学科自我效能感，其中学科能力又可以区分为当前学科成绩和学科发展潜力。《优势学科测评系统》和《学业分析系统》分别对应着学科优势的几个成分。其中，《优势学科测评系统》测量学科兴趣、学科发展潜力和学科自我效能感。《学业分析系统》测量当前学科成绩水平。在选课前认真完成这两个测评即可对自己的学科优势获得了解。</p>
	    <p><strong>第二，未来理想职业。</strong>每个人在选课前最好对自己未来的职业选择有基本的判断。为此需要提前完成以下几项工作。一是全面地认识个人的发展要素，包括兴趣爱好、性格倾向、能力特点、家庭经济条件、学生身体状况、社会关系及家庭背景、发展机遇等；二是大致了解未来人才市场变化趋势，对产业发展趋势有基本的认识。时代和社会的不断发展进步导致产业、行业和职业也在不断发展。这些可以通过资料收集、分析来获得大致的了解。三是要认识不同的职业，每一个职业都是责、权、利的统一体，都是有利有弊的。对于自己的理想职业最好有短暂的职业体验经验。</p>
	    <p><strong>第三，目标专业。</strong>专业与职业之间存在着一对一、多对一、一对多的复杂关联关系。根据职业倒推目标专业时要注意区分硬门槛职业和软门槛职业。如果是硬门槛职业，基本就确定了专业选择。如果是软门槛职业，就能对应比较宽泛的专业选择。可以进一步根据自己的兴趣、能力特征来缩小专业选择范围。</p>
	    <p><strong>第四，高校专业限考要求。</strong>2017年，拟在浙江省招生的全国1368所高校公布了各自的选考科目范围，涵盖了2.37万余个专业。虽然500余所高校没有提出选考要求，但在各高校所有专业中，设限选考科目仍占有将近一半的比例。其中设限范围为1门的占5%，2门的占8%，3门的占33%。北京大学32个专业里，有四成专业要求一门选考科目，比如生物学科类要求选考生物；心理学、计算机等专业要求选考物理。浙江大学的24个专业里，多数要求选考物理。即使是同一类专业，高校要求也不尽相同。如临床医学专业，上海交大要求的限考科目为物理、化学，南开大学则要求限考化学、生物。因此，提前确定自己专业选择的大方向，并了解各学校的专业限考要求，对于科学选课也十分重要。</p>
	  <p>综上，要科学选课，首先，你要站在理想的高度上给自己未来的发展方向定位。其次，既要满足自己的学科兴趣，也要发挥自己的特长，同时要兼顾社会发展需求。</p>
	  <p>在理清各影响因素之后，你可以采用决策平衡单，给每个因素设置一定的权重，认真分析，将它们有机结合，才可以选出既能发挥自己优势又学得开心的学科。</p>
	  <p>下面就是《学业分析系统》对你当前学习成绩信息的全面挖掘。</p>
	  <h2>第一部分 学业分析各指标介绍</h2>
	    <p>学业分析系统对个人在高中阶段历次考试的各科成绩进行数据挖掘与统计分析。它包括了横向数据分析与纵向数据分析两种。横向分析主要是通过将你的成绩在各项指标上与同年级的其他同学进行比较。纵向分析是对你每一学科成绩的历史数据进行分析，揭示学科成绩的变化情况。两者的比较结果都会以图表和文字的形式进行说明。</p>
	    <p>在新高考中，每个学生参加高考录取的3门等级考试科目是不同的，各个科目的考试成绩不能直接相加，必须进行相应的等级转换。参照已有的等级分数计算标准，将每个科目的得分划分为21个等级，第1—7个等级所占的人数比例为28%，转换成的分数在82分到满分之间，这部分学生成绩属于优良。第8—14个等级所占人数的比例为50%，赋分将在61分至79分之间，这部分学生的成绩属于中等。第15—21个等级的人数比例占比22%，转换后的分数在60分以下，这部分学生的成绩属于较差。所以，要想取得高分，选考科目在全省（市）选考学生中的“排名”十分重要。</p>
	  <p>因为无法获取全省（市）统考的数据，我们以全年级同学作为对象进行分析，这个结果代表了你在本校全年级同学中占据的位置。在推测全省（市）的学科排名时，你可以综合考虑本校每个学科在全省（市）的实力（可以从近三年的高考学科平均成绩获得）。如果你在两门学科上的全年级的排名接近，则优先选择本校实力较强的学科。</p>
	  <p>为了挖掘横向和纵向分析的信息，系统主要计算了三项指标。</p>
	  <p>（1）学科成绩的相对水平。这一信息以标准分数为指标，结合试卷的难度系数，呈现了你的学科成绩在全年级的相对水平。</p>
		<p>（2）学科成绩的稳定性。这一信息以标准差为指标，呈现了你某一学科历次考试成绩的稳定性状况。</p>
		<p>（3）学科成绩的趋势，这一信息以趋势线为指标。</p>
		<p>以上三个指标中，第一项为反映当前学科成绩的最根本指标。在考虑第一项指标之后，需要补充考虑第二、三项指标，获得对当前学科成绩更全面的认识。</p>
	  <p>上述三项指标仍然不足以断定选出来的学科一定在高考中具有优势，因为以上三项指标计算的假设是所有同学都参加学科考试。然而事实上，每一门学科只有部分学生会参加考试。所以，你还需要补充考虑全年级中每一种学科组合可能选择的学生人数及你在他们中的相对位置。</p>
	  <p>最后，如果你还未能确定自己的目标专业，建议尽量避免选择专业选择范围较窄的学科组合。</p>
	    <h2>第二部分  学业分析具体结果</h2>
	    <div class="table_a3">
	        <p><strong>1、学科组合相对水平</strong></p>
<!-- 	        <p id = "com1" style = "width:100%;height:500px;"></p> -->
	        <div class="overflow_div">
				<div id="com1" style="text-align:center;width:1920px;height:500px; margin-left:-150px" >
					
				</div>
			</div>
	        <p>推荐学科组合：<span id = "comLevel"><span></p>
	        <p><strong>2、学科组合稳定性</strong></p>
<!-- 	        <p id = "com2" style = "width:100%;height:500px;"></p> -->
	         <div class="overflow_div">
				<div id="com2" style="text-align:center;width:1920px;height:500px; margin-left:-150px" >
					
				</div>
			</div>
	        <p>推荐学科组合：<span id = "comSteady"><span></p>
	    </div>
	    
	    
	    <p><strong>3、科目成绩走向</strong></p>
	    <table class="table_a1">
	    	<tr>
	        	<td>
	            	<p>物理</p>
	            	<div id ="com3" style = "width:500px;height:300px;"></div>
	            </td>
	            <td>
	            	<p>化学</p>
	            	<div id ="com4" style = "width:500px;height:300px;"></div>
	            </td>
	        </tr>
	    	<tr>
	        	<td>
	            	<p>生物</p>
	            	<div id ="com5" style = "width:500px;height:300px;"></div>
	            </td>
	            <td>
	            	<p>历史</p>
	            	<div id ="com6" style = "width:500px;height:300px;"></div>
	            </td>
	        </tr>
	    	<tr>
	        	<td>
	            	<p>地理</p>
	            	<div id ="com7" style = "width:500px;height:300px;"></div>
	            </td>
	            <td>
	            	<p>政治</p>
	            	<div id ="com8" style = "width:500px;height:300px;"></div>
	            </td>
	        </tr>
	        <tr><td colspan = '2'>推荐学科：<span  id = "sub"></span></td></tr>
	    </table>
	
		<p><strong>4、学科组合选课人数预测</strong></p>
	    <table class="table_a2" width="100%" id = "contentTable">
	        <tr>
				<th>学科组合</th>
				<th>预计本校选课学生数</th>
				<th>年级占比</th>
				<th>在选课学生中的成绩排名</th> 
			</tr>
	    </table>
	    
	    <p><strong>5、学科组合可报考的专业比例</strong></p>
	    
	    <table class="table_a2" width="100%">
	    	<tr>
	        	<th>序号</th>
	            <th>组合</th>
	            <th>可报专业比例</th>
	        	<th>序号</th>
	            <th>组合</th>
	            <th>可报专业比例</th>
	        </tr>
	        <tr>
	        	<td>1</td>
	            <td>物理+化学+历史</td>
	            <td>99.9%</td>
	        	<td>11</td>
	            <td>化学+生命科学+历史</td>
	            <td>88.7%</td>
	        </tr>
	        <tr>
	        	<td>2</td>
	            <td>物理+化学+地理</td>
	            <td>99.4%</td>
	        	<td>12</td>
	            <td>化学+政治+历史</td>
	            <td>88.6%</td>
	        </tr>
	        <tr>
	        	<td>3</td>
	            <td>物理+生命科学+历史</td>
	            <td>99.3%</td>
	        	<td>13</td>
	            <td>化学+生命科学+政治</td>
	            <td>88.1%</td>
	        </tr>
	        <tr>
	          <td>4</td>
	          <td>物理+政治+历史</td>
	          <td>99.3%</td>
	          <td>14</td>
	          <td>化学+政治+地理</td>
	          <td>88.1%</td>
	        </tr>
	        <tr>
	          <td>5</td>
	          <td>物理+历史+地理</td>
	          <td>99.3%</td>
	          <td>15</td>
	          <td>化学+历史+地理</td>
	          <td>88.6%</td>
	        </tr>
	        <tr>
	          <td>6</td>
	          <td>物理+生命科学+政治</td>
	          <td>99.0%</td>
	          <td>16</td>
	          <td>化学+生命科学+地理</td>
	          <td>87.4%</td>
	        </tr>
	        <tr>
	          <td>7</td>
	          <td>物理+生命科学+地理</td>
	          <td>99.0%</td>
	          <td>17</td>
	          <td>生命科学+历史+地理</td>
	          <td>77.8%</td>
	        </tr>
	        <tr>
	          <td>8</td>
	          <td>物理+化学+地理</td>
	          <td>99.0%</td>
	          <td>18</td>
	          <td>生命科学+政治+地理</td>
	          <td>76.1%</td>
	        </tr>
	        <tr>
	          <td>9</td>
	          <td>物理+化学+政治</td>
	          <td>98.9%</td>
	          <td>19</td>
	          <td>生命科学+政治+历史</td>
	          <td>75.3%</td>
	        </tr>
	        <tr>
	          <td>10</td>
	          <td>物理+化学+生命科学</td>
	          <td>97.4%</td>
	          <td>20</td>
	          <td>政治+历史+地理</td>
	          <td>52.9%</td>
	        </tr>
	    </table>
	</div>
</body>
</html>