<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
<title>学科组合相对水平</title>
<meta name="decorator" content="default"/>
<script src="${ctxStatic}/echart/echarts.js" type="text/javascript"></script>
<style>


.overflow_div{
width:1280px;
overflow-x:auto;
margin:0 auto;
}	
</style>

<script type = "text/javascript">
$(document).ready(function() {
	var url = "echart2";
	$.ajax({
		url:url,
		dataType:"json",
		type:"POST",
		success:function(data){
			var x = new Array();
			var y = new Array();
			$("#count").html(data.count+"  &nbsp;&nbsp;&nbsp;");
			$("#subject").html(data.best.substring(0,data.best.length-1));
			$.each(data,function(key,value){
				if(key!= 'count' && key != 'best'){
					x.push(key);
					y.push(value);
				}
			});
			createEchart(x,y,data.count)
		},
		error:function(){
		}
	})	
})
function createEchart(x,y,count){
	var yy = [];
	for(var i = 0;i<y.length;i++){
		yy[i] = count - y[i];
	}
	var dom = document.getElementById("contentChart");
	var myChart = echarts.init(dom);
	var app = {};
	option = null;
	option = {
	    xAxis: {
	        type: 'category',
	        data: x,
	        name:'科目组合',
	        interval: 0
	    },
	    yAxis: {
	        type: 'value',
	        name:'学年名次',
	        axisLabel : {
                formatter: function(v){
                    return count-v +1;
                }
            }
	    },
	    series: [{
	        data: yy,
	        type: 'bar',
	        label:{ 
	            normal:{ 
		            show: true, 
		            position: 'top',
		            formatter: function(params) { 
		            	for (var i = 0, l = option.xAxis.data.length; i < l; i++) {  
		            		if (option.xAxis.data[i] == params.name) {  
		            			 var val1 = count - params.value +1;
		            			 return val1;  
		            		}
		            	}
		            }
	            } 
	         }
	    }]
	};
	;
	if (option && typeof option === "object") {
	    myChart.setOption(option, true);
	}
}

</script>
</head>
<body>
	<ul class="nav nav-tabs" style="float:left;">
		<li class="active"><a href="${ctx}/adviceStu/controller/comRelative">学科组合相对水平</a></li>
	</ul>
	<div style="clear:both"></div>
	<div  style = "text-align:center;">年级学生数：<span id = "count"></span>推荐学科组合：<span id = "subject"></span>
	</div>
	<div style="clear:both"></div>
	<div class="overflow_div">
		<div id="contentChart" style="text-align:center;width:1920px;height:500px; margin-left:-150px" >
			
		</div>
	</div>
</body>
</html>