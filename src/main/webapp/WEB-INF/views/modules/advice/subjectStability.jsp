<%@ page contentType="text/html;charset=UTF-8" %>
<%@ include file="/WEB-INF/views/include/taglib.jsp"%>
<html>
<head>
<title>学科稳定性</title>
<meta name="decorator" content="default"/>
<script src="${ctxStatic}/echart/echarts.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function() {
	var url = "echart3";
	$.ajax({
		url:url,
		dataType:"json",
		type:"POST",
		success:function(data){
			var x = new Array();
			var y = new Array();
			console.log(data)
			$("#count").html(data.count+"  &nbsp;&nbsp;&nbsp;");
			$("#subject").html(data.best.substring(0,data.best.length-1))
// 			$.each(data,function(key,value){
// 				if(key!= 'count' && key != 'best'){
// 					x.push(key);
// 					y.push(value);
// 				}
// 			});
			createEchart(data.x,data.y,data.count)
		},
		error:function(){
			
		}
	})
})
function createEchart(x,y,count){
	var dom = document.getElementById("contentChart");
	var myChart = echarts.init(dom);
	var app = {};
	option = null;
	option = {
	    xAxis: {
	        type: 'category',
	        data: x,
	        name:'科目'
	    },
	    yAxis: {
	        type: 'value',
	        name:'名次波动排名',
	        axisLabel : {
                formatter: function(v){
                    return 6 - v;
                }
            }
	    },
	    series: [{
	        data: y,
	        type: 'bar',
	        label:{ 
	            normal:{ 
		            show: true, 
		            position: 'top',
		            formatter: function(params) { 
		            	 for (var i = 0,l = option.xAxis.data.length; i < l; i++) { 
		            		 if (option.xAxis.data[i] == params.name) {  
		            			 var val1 = 6 - params.value  ;  
		            			 return val1;  
		            		 }
		            	 }
		            }
	            } 
	         }
	    }]
	};
	;
	if (option && typeof option === "object") {
	    myChart.setOption(option, true);
	}
}

</script>
</head>
<body>
	<ul class="nav nav-tabs">
		<li class="active"><a href="${ctx}/adviceStu/controller/subStability">学科稳定性</a></li>
	</ul>
	<div  style = "float:right;position:relative;right:30%;">年级学生数：<span id = "count"></span>推荐科目：<span id = "subject"></span>
	</div>
	<div id="contentChart" style="text-align:center;width:90%;height:500px;" >
		
	</div>
</body>
</html>