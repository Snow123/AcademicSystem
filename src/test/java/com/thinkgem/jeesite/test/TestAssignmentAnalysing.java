package com.thinkgem.jeesite.test;
import com.thinkgem.jeesite.modules.ans.AssignmentAnalyser;

import org.junit.Test;
import static org.junit.Assert.*;

// Used to test algorithm
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;
import org.apache.commons.math3.util.Precision;
import org.nd4j.linalg.indexing.NDArrayIndex;

public class TestAssignmentAnalysing {

    public float[] fullScores = new float[]{
            15, 15, 26, 20, 10, 14
    };
    public INDArray FullScores = Nd4j.create(fullScores);

    private float df[] = {0.21f, 0.25f, 0.25f, 0.48f, 0.15f, 0.27f};
    private float taos[] = {0.25f, 0.05f, 0.39f, 0.53f, 0.29f, 0.58f};

    public float scores[][] = new float[][]{
            {15,13,26,20,10,14},
            {14,13,24,20,10,11},
            {14,13,26,15,10,14},
            {14,10,24,20,10,13},
            {15,13,26,15,10,10},
            {12,12,24,18,10,12},
            {14,12,20,18,8,14},
            {15,11,26,15,8,10},
            {15,14,20,15,10,10},
            {13,10,24,12,10,14},
            {11,11,24,12,10,14},
            {14,9,26,10,8,14},
            {14,10,26,10,10,14},
            {15,14,24,2,10,14},
            {12,11,14,10,10,12},
            {13,10,23,18,8,6},
            {14,11,20,10,10,12},
            {12,14,22,5,10,14},
            {12,12,18,12,10,12},
            {13,13,20,10,5,14},
            {10,13,18,15,8,10},
            {13,12,18,10,6,14},
            {12,11,26,5,10,8},
            {10,13,16,15,10,8},
            {10,13,14,10,10,14},
            {11,10,20,10,10,10},
            {12,8,18,12,6,14},
            {12,10,16,10,8,14},
            {15,11,26,5,8,5},
            {10,11,16,15,5,12},
            {9,10,18,7,10,14},
            {13,10,20,0,10,13},
            {13,10,20,0,10,13},
            {8,8,16,10,10,14},
            {8,9,26,7,6,9},
            {7,11,10,15,10,10},
            {13,11,19,10,8,2},
            {9,12,16,7,10,8},
            {12,14,18,7,8,2},
            {12,10,16,10,10,2},
            {11,11,18,10,2,5},
            {9,11,10,12,8,2},
            {11,12,10,4,8,5},
            {7,11,12,0,8,10},
            {10,11,10,0,5,5},
            {10,11,10,2,0,0}
    };
    public INDArray Scores = Nd4j.create(scores);
    @Test
    public void TestReliabilityAlg(){
        // N is students , n is problems
        // Nxn

        INDArray Sj = Scores.std(0);
        Sj = Sj.mul(Sj);
        float sum_sj = Sj.sumNumber().floatValue();
        float s = Scores.sum(1).stdNumber().floatValue();
        s = s * s;
        float n = (float)Scores.columns();
        float R = n / (n-1) * (1 - sum_sj/s);
        assertEquals(0.64f, Precision.round(R, 2), 1e-4);
    }

    @Test
    public void TestDifficultyAlg(){

        INDArray means = Scores.mean(0);
        INDArray Dj = Nd4j.ones(Scores.columns()).sub(means.div(FullScores));

        for(int i = 0; i < df.length; i++)
            assertEquals(df[i], Precision.round(Dj.getFloat(i), 2), 1e-4);

        float D = 0.01f * Dj.mul(FullScores).sumNumber().floatValue();

        assertEquals(0.28f, Precision.round(D, 2), 1e-4);
    }

    @Test
    public void TestDifferentiationAlg(){

        int columnNum = Scores.columns();
        int rowNum = Scores.rows();
        final int num = 10;

        INDArray sorted_idx[] = Nd4j.sortWithIndices(
                Nd4j.sum(Scores, 1), 0, false);

        INDArray sorted = Scores.get(NDArrayIndex.create(sorted_idx[0]));
        INDArray hGroup = sorted.get(NDArrayIndex.interval(0, num), NDArrayIndex.all());
        INDArray lGroup = sorted.get(NDArrayIndex.interval(rowNum - num, rowNum), NDArrayIndex.all());

        INDArray Hj = Nd4j.zeros(columnNum);
        INDArray Lj = Nd4j.zeros(columnNum);
        for(int i = 0; i < columnNum; i++){
            INDArray hCol = hGroup.getColumn(i);
            INDArray lCol = lGroup.getColumn(i);
            Hj.putScalar(i, hCol.meanNumber().floatValue());
            Lj.putScalar(i, lCol.meanNumber().floatValue());
        }
        INDArray Taoj = Hj.sub(Lj).div(FullScores);

        for(int i = 0; i < taos.length; i++)
            assertEquals(taos[i], Precision.round(Taoj.getFloat(i), 2), 1e-4);

        float tao = .01f * Taoj.mul(FullScores).sumNumber().floatValue();
        assertEquals(0.36f, Precision.round(tao, 2), 1e-4);
    }

    @Test
    public void TestReliabilityLib(){
        AssignmentAnalyser ans = new AssignmentAnalyser(fullScores, scores);
        float R = ans.getReliability();
        assertEquals(0.64, R, 1e-4);
    }

    @Test
    public void TestDifficultyLib(){
        AssignmentAnalyser ans = new AssignmentAnalyser(fullScores, scores);
        float D = ans.getDifficulty();
        assertEquals(0.28f, D, 1e-4);
    }

    @Test
    public void TestDifferentiationLib(){
        AssignmentAnalyser ans = new AssignmentAnalyser(fullScores, scores);
        float tao = ans.getDifferentiation(10);
        assertEquals(0.36f, tao, 1e-4);
    }
}
