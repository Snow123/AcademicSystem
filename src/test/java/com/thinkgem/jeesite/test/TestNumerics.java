package com.thinkgem.jeesite.test;
import com.thinkgem.jeesite.modules.ans.NumericUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import static org.junit.Assert.*;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.factory.Nd4j;


public class TestNumerics {
    public float scores[][] = new float[][]{
            {15,13,26,20,10,14},
            {14,13,24,20,10,11},
            {14,13,26,15,10,14},
            {14,10,24,20,10,13},
            {15,13,26,15,10,10},
            {12,12,24,18,10,12},
            {14,12,20,18,8,14},
            {15,11,26,15,8,10},
            {15,14,20,15,10,10},
            {13,10,24,12,10,14},
            {11,11,24,12,10,14},
            {14,9,26,10,8,14},
            {14,10,26,10,10,14},
            {15,14,24,2,10,14},
            {12,11,14,10,10,12},
            {13,10,23,18,8,6},
            {14,11,20,10,10,12},
            {12,14,22,5,10,14},
            {12,12,18,12,10,12},
            {13,13,20,10,5,14},
            {10,13,18,15,8,10},
            {13,12,18,10,6,14},
            {12,11,26,5,10,8},
            {10,13,16,15,10,8},
            {10,13,14,10,10,14},
            {11,10,20,10,10,10},
            {12,8,18,12,6,14},
            {12,10,16,10,8,14},
            {15,11,26,5,8,5},
            {10,11,16,15,5,12},
            {9,10,18,7,10,14},
            {13,10,20,0,10,13},
            {13,10,20,0,10,13},
            {8,8,16,10,10,14},
            {8,9,26,7,6,9},
            {7,11,10,15,10,10},
            {13,11,19,10,8,2},
            {9,12,16,7,10,8},
            {12,14,18,7,8,2},
            {12,10,16,10,10,2},
            {11,11,18,10,2,5},
            {9,11,10,12,8,2},
            {11,12,10,4,8,5},
            {7,11,12,0,8,10},
            {10,11,10,0,5,5},
            {10,11,10,2,0,0}
    };

	@Test
	public void TestMean(){
        INDArray arr = Nd4j.ones(10);
		assertEquals(1.0, arr.meanNumber().floatValue(), 1e-6);
	}

	@Test
    public void TestStd(){
	    INDArray arr = Nd4j.ones(10);
	    assertEquals(arr.stdNumber(true).floatValue(), 0.0, 1e-6);
    }
	@Test
    public void TestRegression(){
        float[] x = {1.0f};
        float[] y = {1.0f};
        float[] params = NumericUtils.LinearRegression(x, y);
        assertEquals(0.0, params[0], 1e-4);
        assertEquals(0.0, params[1], 1e-4);

        x = NumericUtils.Range(scores[0].length);
        y = scores[0];

        params = NumericUtils.LinearRegression(x, y);
        assertEquals(-0.57, params[0], 1e-2);
        assertEquals(18.33, params[1], 1e-2);
	}

	@Test
	public void TestArgsort(){
        INDArray scores = Nd4j.create(this.scores);
        
        ImmutablePair<int[][], float[][]> indices2d
                = NumericUtils.ArgSort(this.scores, 0, true);
        int[][] I = indices2d.left;

        assertEquals(46, I.length);
        assertEquals(0, I[45][4]);
        assertEquals(0, I[45][5]);

        indices2d = NumericUtils.ArgSort(this.scores, 1, true);
        I = indices2d.left;
        assertEquals(46, I.length);

        // I[0] = {3, 1, 5, 4, 0, 2}
        assertArrayEquals(new int[]{3,1,5,4,0,2}, I[0]);
    }

    @Test
    public void TestArgMax(){
        int[] indices = NumericUtils.ArgMax(scores);
        assertEquals(indices.length, 46);
        assertEquals(2, indices[0]);
        assertEquals(1, indices[45]);
    }
}
