package com.thinkgem.jeesite.test;

import com.thinkgem.jeesite.modules.ans.DbUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import org.apache.commons.lang3.tuple.ImmutablePair;
import org.junit.Test;
import static org.junit.Assert.*;
import com.mysql.jdbc.Driver;
import oracle.jdbc.OracleDriver;


public class TestDbUtils{
    public static final String URL = "jdbc:mysql://localhost:3306/media";
    public static final String USER = "root";
    public static final String PASSWORD = "898213141310";
    public Connection connection;

    public TestDbUtils() throws SQLException{
        connection = DriverManager.getConnection(URL, USER, PASSWORD);
    }

    @Test
    public void TestGetSubjectNum() throws SQLException{
        DbUtils util = new DbUtils(connection);
        int[] subjects = util.getSubjectsByExam(1);
        assertEquals(8, subjects.length);
        for(int i = 0; i < 8; i++)
            assertEquals(i+1, subjects[i]);
    }

    @Test
    public void TestGetSubjectNum2() throws SQLException{
        DbUtils util = new DbUtils(connection);
        int[] subjects = util.getSubjectsByStudent(1);
        assertEquals(8, subjects.length);
        for(int i = 0; i < 8; i++)
            assertEquals(i+1, subjects[i]);
    }

    @Test
    public void TestGetScoresByExam(){
        DbUtils util = new DbUtils(connection);
        ImmutablePair<int[],float[][]> result = util.getScoresByExam(1);
        assertNotNull(result);
        assertNotNull(result.left);
        assertNotNull(result.right);
        assertEquals(result.left.length, result.right.length);
    }

    @Test
    public void TestGetScoresByStudent(){
        DbUtils util = new DbUtils(connection);
        ImmutablePair<int[],float[][]> result = util.getScoresByStudent(1);
        assertNotNull(result);
        assertNotNull(result.left);
        assertNotNull(result.right);
        assertEquals(result.left.length, result.right.length);
    }

    @Test
    public void TestSaveNormalized(){
        int[] ids = new int[]{0, 1, 2, 3};
        float[][] scores = new float[][]{
            {1.f, 2.f, 3.f, 4.f, 5.f, 6.f, 7.f, 8.f},
            {9.f, 10.f, 11.f, 12.f, 13.f, 14.f, 15.f, 16.f},
            {17.f, 18.f, 19.f, 20.f, 21.f, 22.f, 23.f, 24.f},
            {25.f, 26.f, 27.f, 28.f, 29.f, 30.f, 31.f, 32.f}
        };
        DbUtils util = new DbUtils(connection);
        util.saveNormalizedScores(1, new ImmutablePair<int[],float[][]>(ids, scores));
    }
}
